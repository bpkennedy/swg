import Vue from "vue"
import Vuex from "vuex"
import { api } from './api'

Vue.use(Vuex);

export const LOAD_ALL_PLANETS_ACTION = 'LOAD_ALL_PLANETS_ACTION'
export const SET_LOADING_ACTION = 'SET_LOADING_ACTION'
const SET_LOADING_MUTATION = 'SET_LOADING_MUTATION'
const SET_PLANETS_MUTATION = 'SET_PLANETS_MUTATION'

export default new Vuex.Store({
  state: {
    loading: false,
    planets: [],
  },
  actions: {
    async [SET_LOADING_ACTION]({commit}, status) {
      commit(SET_LOADING_MUTATION, status)
    },
    async [LOAD_ALL_PLANETS_ACTION]({commit, dispatch}) {
      dispatch(SET_LOADING_ACTION, true)
      const response = await api.get('planets')
      commit(SET_PLANETS_MUTATION, response.data)
    },
  },
  mutations: {
    [SET_LOADING_MUTATION](state, status) {
      Vue.set(state, 'loading', status)
    },
    [SET_PLANETS_MUTATION]({state, dispatch}, data) {
      console.log(data)
      Vue.set(state, 'planets', [...data])
      dispatch(SET_LOADING_ACTION, false)
    },
  },
});
