import axios from 'axios'

export function apiUrl() {
  return `http://${window.location.host}/api/v1/`
}

export const api = axios.create({
  baseURL: apiUrl()
})

export function addApiErrorHandlerInterceptor(callback) {
  api.interceptors.response.use(function(response) {
    return response
  }, function(error) {
    callback(error)
    return Promise.reject(error)
  })
}
