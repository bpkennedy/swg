import { errorExit, gracefulExit } from './helper'

let simulation = null
let intentionalExit = false

function update() {
  try {
    console.log(new Date())
  } catch(error) {
    errorExit(error, simulation)
  }
}

function loop() {
  if (intentionalExit) {
    gracefulExit(simulation)
  }
  update()
}

simulation = setInterval(loop, 2000)
