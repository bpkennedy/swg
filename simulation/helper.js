export const errorExit = (error, simulation) => {
  console.error(error)
  clearInterval(simulation)
  process.exit(1)
}

export const gracefulExit = (simulation) => {
  console.log('Simulation exited gracefully.')
  clearInterval(simulation)
  process.exit()
}