const { createOrganizationTypesTableIfNotExists } = require('../../helper')

const organizationTypes = [
  {
    createdAt: new Date(),
    name: 'Government',
    description: "Politics were the activities associated with the policies of a government. Those involved in such activities were referred to as politicians. Across the galaxy, there existed many different forms of government.",
  },
  {
    createdAt: new Date(),
    name: 'Commerce',
    description: "The galactic economy was the macroeconomic combination of sector, system and planetary production, consumption, and trade. It could be broken down and represented in various ways, but it was inseparable from the astrography and ecological make up of the galaxy. It is important to note that economic differences between major galactic eras were negligible.",
  },
  {
    createdAt: new Date(),
    name: 'Agriculture',
    description: "Farmers were farm workers or owners. On desert worlds like Tatooine, moisture farmers harvested water from the atmosphere.",
  },
  {
    createdAt: new Date(),
    name: 'Shipyard',
    description: "A shipyard was a place where starships were either constructed or repaired, typically located in orbit around a planet.",
  },
  {
    createdAt: new Date(),
    name: 'Construction',
    description: "",
  },
  {
    createdAt: new Date(),
    name: 'Media',
    description: "",
  },
  {
    createdAt: new Date(),
    name: 'Manufacturing',
    description: "",
  },
  {
    createdAt: new Date(),
    name: 'Medical',
    description: "",
  },
  {
    createdAt: new Date(),
    name: 'Mining',
    description: "",
  },
  {
    createdAt: new Date(),
    name: 'Salvage',
    description: "",
  },
  {
    createdAt: new Date(),
    name: 'Scouting',
    description: "",
  },
  {
    createdAt: new Date(),
    name: 'Security',
    description: "",
  },
  {
    createdAt: new Date(),
    name: 'Transport',
    description: "",
  },
  {
    createdAt: new Date(),
    name: 'Religion',
    description: "",
  },
  {
    createdAt: new Date(),
    name: 'Criminal',
    description: "",
  },
]

exports.seed = async knex => {
  await createOrganizationTypesTableIfNotExists(knex)
  await knex('organizationTypes').insert(organizationTypes)
};
