const { createPlanetsTableIfNotExists } = require("../../helper");

const planets = [
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Prakith",
    grid: "K10",
    x: -668.68,
    y: -1332.3
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Keeara Major",
    grid: "K10",
    x: -110.05,
    y: -875.28
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Symbia",
    grid: "K10",
    x: -152.78,
    y: -778.16
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Kuar",
    grid: "K10",
    x: -28.47,
    y: -678.46
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Odik",
    grid: "K11",
    x: -1152.75,
    y: -1646.85
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Byss",
    grid: "K11",
    x: -721.1,
    y: -2731.02
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Kalist",
    grid: "K12",
    x: -204.62,
    y: -3034.84
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Zamael",
    grid: "K12",
    x: -201.17,
    y: -3198.86
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Lialic",
    grid: "K12",
    x: -249.51,
    y: -3402.59
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Constancia",
    grid: "K12",
    x: -210.88,
    y: -3632.69
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Dulvoyinn",
    grid: "K12",
    x: -51.61,
    y: -3703.9
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Tython",
    grid: "L10",
    x: 19.19,
    y: -1173.87
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Had Abbadon",
    grid: "L10",
    x: 1115.53,
    y: -1182.51
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Cambria",
    grid: "L10",
    x: 1396.66,
    y: -876.72
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Vulpter",
    grid: "L10",
    x: 876.1,
    y: -735.67
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Besero",
    grid: "L10",
    x: 744.02,
    y: -572.52
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Primus Goluud",
    grid: "L10",
    x: 547.19,
    y: -541.44
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Empress Teta",
    grid: "L10",
    x: 29.8,
    y: -782.05
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Iope",
    grid: "L10",
    x: 173.53,
    y: -524.37
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Starswarm Cluster",
    grid: "L10",
    x: 146.34,
    y: -599.47
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Jerrilek",
    grid: "L10",
    x: 29.19,
    y: -360.77
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Tsoss Beacon",
    grid: "L11",
    x: 171.13,
    y: -1579.61
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Eclipse",
    grid: "L11",
    x: 1488.68,
    y: -2654.1
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Crystan",
    grid: "L12",
    x: 201.08,
    y: -3703.28
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Khomm",
    grid: "L12",
    x: 187.61,
    y: -3827.06
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Dremulae",
    grid: "M10",
    x: 1526.48,
    y: -1102.89
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Ojom",
    grid: "M11",
    x: 1617.85,
    y: -2449.18
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Ottabesk",
    grid: "M11",
    x: 1693.6,
    y: -2388.97
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Hakassi",
    grid: "M11",
    x: 1699.53,
    y: -2292.17
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Ebaq",
    grid: "M11",
    x: 1592.06,
    y: -2148.44
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Thoadeye",
    grid: "M11",
    x: 1784.08,
    y: -1684.92
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Galactic Center",
    grid: 0,
    x: -2131.37,
    y: 0
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Kokash",
    grid: "J10",
    x: -1654.62,
    y: -982.53
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Pollillus",
    grid: "J10",
    x: -1519.95,
    y: -958.36
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Osssorck Nebulae",
    grid: "J12",
    x: -1523.85,
    y: -4003.65
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Questal",
    grid: "J12",
    x: -1766.12,
    y: -4107.22
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Kitel Phard",
    grid: "J12",
    x: -1989.9,
    y: -4301.41
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Inysh",
    grid: "J12",
    x: -1779.07,
    y: -4436.42
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Dahrtag",
    grid: "J13",
    x: -1584.18,
    y: -4721.92
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Metellos",
    grid: "K9",
    x: -202.62,
    y: 18.42
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Xa Fel",
    grid: "K9",
    x: -1390.48,
    y: 495.69
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Ragoon",
    grid: "K9",
    x: -1226.02,
    y: 241.89
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Merakai",
    grid: "K9",
    x: -925.83,
    y: 769.32
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Denevar",
    grid: "K9",
    x: -729.16,
    y: 793.35
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Pantolomin",
    grid: "K9",
    x: -558.4,
    y: 666.62
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Dachat",
    grid: "K9",
    x: -888.17,
    y: 568.2
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Voon",
    grid: "K9",
    x: -556.67,
    y: 552.67
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Hyabb",
    grid: "K9",
    x: -701.7,
    y: 468.07
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Farrfin",
    grid: "K9",
    x: -377.45,
    y: 780.47
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Twith",
    grid: "K9",
    x: -427.52,
    y: 638.9
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Scipio",
    grid: "K5",
    x: -967.99,
    y: 7229.36
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Galvoni",
    grid: "K9",
    x: -225.52,
    y: 443.8
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Weerden",
    grid: "K9",
    x: -206.53,
    y: 271.15
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Tanjay",
    grid: "K9",
    x: -106.39,
    y: 176.19
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Mamendin",
    grid: "K9",
    x: -139.19,
    y: 880.61
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Tamban",
    grid: "K10",
    x: -1428.45,
    y: -682.11
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Aradia",
    grid: "K10",
    x: -1354.21,
    y: -525
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Cal-Seti",
    grid: "K10",
    x: -1109.04,
    y: -512.91
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "N'Zoth",
    grid: "K10",
    x: -1435.35,
    y: -866.51
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Fresia",
    grid: "K10",
    x: -872.51,
    y: -442.13
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Galand",
    grid: "K10",
    x: -803.45,
    y: -355.8
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Alland",
    grid: "K10",
    x: -689.5,
    y: -215.95
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Worru'du",
    grid: "K10",
    x: -579,
    y: -267.75
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Salliche",
    grid: "K10",
    x: -385.63,
    y: -278.11
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Norkronia",
    grid: "K10",
    x: -501.31,
    y: -102.43
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Stassia",
    grid: "K10",
    x: -199.16,
    y: -171.49
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Foerost",
    grid: "L10",
    x: -0.61,
    y: -111.07
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Ruan",
    grid: "K10",
    x: -0.61,
    y: -207.75
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Galantos",
    grid: "K10",
    x: -1418.65,
    y: -837.82
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "J't'p'tan",
    grid: "K10",
    x: -1449.08,
    y: -848.4
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Botor",
    grid: "K12",
    x: -57.93,
    y: -4056.06
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Thracior",
    grid: "K12",
    x: -179.64,
    y: -4139.74
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Thebeon",
    grid: "K12",
    x: -471.53,
    y: -4488.2
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Gerrard",
    grid: "K12",
    x: -51.71,
    y: -4429.02
  },
  {
    createdAt: new Date(),
    governmentId: 1,
    name: "Daupherm",
    grid: "K12",
    x: -277.34,
    y: -4005.5
  }
];

exports.seed = async knex => {
  await createPlanetsTableIfNotExists(knex);
  await knex("planets").insert(planets);
};
