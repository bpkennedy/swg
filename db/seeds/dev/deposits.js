const { createDepositsTableIfNotExists } = require('../../helper')

const deposits = [{
  createdAt: new Date(),
  discovererId:1,
  planetId:1,
  materialTypeId:5,
  quantity:500
}]

exports.seed = async knex => {
  await createDepositsTableIfNotExists(knex)
  await knex('deposits').insert(deposits)
};
