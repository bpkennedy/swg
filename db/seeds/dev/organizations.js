const { createOrganizationsTableIfNotExists } = require('../../helper')

const organizations = [
  {
    createdAt: new Date(),
    name: 'New Republic',
    logoUrl: 'https://vignette.wikia.nocookie.net/starwars/images/0/0f/New_Republic_canon.svg/revision/latest?cb=20160126010008',
    description: "The New Republic, also known as simply the Republic, was a democratic government composed of various member worlds spread across the galaxy. It was the restoration of the Galactic Republic, a democratic state that governed the galaxy for a millennium until the rise of the Galactic Empire. Instead of affixing its capital to Coruscant, the galaxy's historical seat of government, each member world would have the chance to host the newly-restored Galactic Senate on a rotating basis. The New Republic's first capital was Chandrila; a planet located in the Core Worlds like Coruscant, it was also the homeworld of Chancellor Mon Mothma, the new government's first head of state. Approximately three decades after the formation of the Republic, the cosmopolitan world of Hosnian Prime was serving as the Senate's headquarters.",
    typeId:1
  },
  {
    createdAt: new Date(),
    name: 'Galactic Empire',
    logoUrl: 'https://vignette.wikia.nocookie.net/starwars/images/2/2e/Imperial_Emblem.svg/revision/latest?cb=20080220004323',
    description: "The Galactic Empire (19 BBY–5 ABY), also known as the First Galactic Empire, the New Order, or as simply the Empire, was the fascist government that replaced the Galactic Republic in the aftermath of the Clone Wars.",
    typeId:1,
  },
  {
    createdAt: new Date(),
    name: 'Sith Order',
    logoUrl: 'https://vignette.wikia.nocookie.net/starwars/images/b/bf/Sith_canon.svg/revision/latest?cb=20160702013109',
    description: "The Sith, also referred to as the Sith Order, was an ancient order of Force-wielders devoted to the dark side of the Force. Driven by their emotions, including hate, anger, and greed, the Sith were deceptive and obsessed with gaining power no matter the cost. The order reached the apex of its power under Darth Sidious, the Dark Lord of the Sith who achieved his order's goal of galactic conquest after a millennia of plotting. Within a generation, however, the death of Darth Vader would mark the end of the order of Sith Lords.",
    typeId:14,
  },
  {
    createdAt: new Date(),
    name: 'Jedi Order',
    logoUrl: 'https://vignette.wikia.nocookie.net/starwars/images/9/9d/Jedi_symbol.svg/revision/latest?cb=20080329163323',
    description: "The Jedi Order was a noble, religious order of protectors united in their devotion to the light side of the Force. With a history dating back thousands of years before the rise of the Galactic Empire, the Jedi Knights—noted for their lightsabers and natural ability to harness the powers of the Force—stood as the guardians of peace and justice in the Galactic Republic. During the Republic Era, the Jedi Temple on the Core World Coruscant served as the hub of all Jedi activity in the galaxy. The Temple was also a training school for younglings and Padawans, who learned the ways of the Force under the supervision of Jedi Masters, twelve of whom were elected to serve on the Jedi High Council—the Order's highest governing authority. The High Council's staff officers were the Grand Master, a title reserved for the wisest Jedi Master, and the Master of the Order, who served as the Order's appointed leader. The Jedi Code governed the Order's way of life; therefore, every Jedi was duty-bound to observe and uphold the Code or else risk banishment from the Order's ranks.",
    typeId:14,
  },
  {
    createdAt: new Date(),
    name: 'Kuat Drive Yards',
    logoUrl: 'https://vignette.wikia.nocookie.net/starwars/images/0/0d/KDY.svg/revision/latest/scale-to-width-down/1000?cb=20080305080702',
    description: "Kuat Drive Yards (KDY) was a starship manufacturer based on the planet Kuat. The main production facility of Kuat Drive Yards was the Kuat Drive Yards Orbital Shipyards, a ring around the planet Kuat. It produced many of the galaxy's most terrifying symbols of Imperial might, including the Imperial-class Star Destroyer, and also produced walkers such as the All Terrain Defense Pod. It had several subsidiaries, with Kuat Vehicles focusing on civilian ground products, and Kuat Systems Engineering producing starfighters.",
    typeId:4,
  },
  {
    createdAt: new Date(),
    name: 'Sienar Fleet Systems',
    logoUrl: 'https://vignette.wikia.nocookie.net/starwars/images/3/39/Sienar.svg/revision/latest/scale-to-width-down/673?cb=20080311192948',
    description: 'Sienar Fleet Systems (SFS) was one of the foremost starship, arms, and drive manufacturers within the galaxy, best known for its TIE line craft during the reign of the Galactic Empire. It was formerly known as Republic Sienar Systems during the Republic Era. The company reorganized into Sienar-Jaemus Fleet Systems and Sienar-Jaemus Army Systems following the Galactic Concordance to avoid arms restrictions against selling weaponry to the First Order.',
    typeId:4,
  },
  {
    createdAt: new Date(),
    name: 'Rendili Stardrive',
    logoUrl: 'https://vignette.wikia.nocookie.net/starwars/images/9/91/Rendili.svg/revision/latest/scale-to-width-down/530?cb=20080322021132',
    description: "Rendili StarDrive was a starship-manufacturing company based on the planet Rendili. One of the ancient Founding Shipwrights, it was the primary supplier of ships to the Republic Navy for millennia until it was eclipsed in the final centuries of the Galactic Republic by Kuat Drive Yards.",
    typeId:4,
  },
  {
    createdAt: new Date(),
    name: 'Corellian Engineering Corporation',
    logoUrl: 'https://vignette.wikia.nocookie.net/starwars/images/6/62/Corellian_Engineering_Corporation.svg/revision/latest/scale-to-width-down/1000?cb=20080312040228',
    description: "The Corellian Engineering Corporation, abbreviated CEC, was a Corellian starship manufacturer. Its products included several models of freighters, such as the YV-666 light freighter, the YT-1300 light freighter, the VCX-100 light freighter, the VCX-150 freighter and the Baleen-class heavy freighter.",
    typeId:4,
  },
]

exports.seed = async knex => {
  await createOrganizationsTableIfNotExists(knex)
  await knex('organizations').insert(organizations)
};
