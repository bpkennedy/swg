exports.up = async function(knex, Promise) {
  await knex.schema.createTable('planetOrganizations', table => {
      table.increments('id').primary()
      table.integer('planetId')
      table.integer('organizationId')
  });
};

exports.down = async function(knex) {
  await knex.schema.dropTable('planetOrganizations');
};