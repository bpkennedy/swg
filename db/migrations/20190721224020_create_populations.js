exports.up = async (knex, Promise) => {
  return await knex.schema.table('planets', (table) => {
    table.integer('population').notNull().defaultTo(0)
  })
}

exports.down = async (knex, Promise) => {
  return await knex.schema.table('planets', (table) => {
    table.dropColumn('population')
  })
}
