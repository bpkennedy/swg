exports.createPlanetsTableIfNotExists = async (knex) => {
  const hasTable = await knex.schema.hasTable('planets')
  if (!hasTable) {
    return await knex.schema.createTable('planets', (t) => {
      t.increments('id').primary()
      t.dateTime('createdAt').notNull()
      t.dateTime('updatedAt').nullable()
      t.dateTime('deletedAt').nullable()
  
      t.integer('governmentId').notNull()
      t.string('name').notNull()
      t.string('grid').notNull()
      t.decimal('x', null).notNull()
      t.decimal('y', null).notNull()
    })
  }
}

exports.createOrganizationsTableIfNotExists = async (knex) => {
  const hasTable = await knex.schema.hasTable('organizations')
  if (!hasTable) {
    return await knex.schema.createTable('organizations', (t) => {
      t.increments('id').primary()
      t.dateTime('createdAt').notNull()
      t.dateTime('updatedAt').nullable()
      t.dateTime('deletedAt').nullable()

      t.string('name').notNull()
      t.text('logoUrl').nullable()
      t.text('description').nullable()
      t.integer('typeId').notNull()
    })
  }
}

exports.createOrganizationTypesTableIfNotExists = async (knex) => {
  const hasTable = await knex.schema.hasTable('organizationTypes')
  if (!hasTable) {
    return await knex.schema.createTable('organizationTypes', (t) => {
      t.increments('id').primary()
      t.dateTime('createdAt').notNull()
      t.dateTime('updatedAt').nullable()
      t.dateTime('deletedAt').nullable()
  
      t.string('name').notNull()
      t.text('description').nullable()
    })
  }
}

exports.createDepositsTableIfNotExists = async (knex) => {
  const hasTable = await knex.schema.hasTable('deposits')
  if (!hasTable) {
    return await knex.schema.createTable('deposits', (t) => {
      t.increments('id').primary()
      t.dateTime('createdAt').notNull()
      t.dateTime('updatedAt').nullable()
      t.dateTime('deletedAt').nullable()

      t.integer('discovererId').notNull()
      t.integer('planetId').notNull()
      t.integer('materialTypeId').notNull()
      t.integer('quantity').notNull()
    })
  }
}