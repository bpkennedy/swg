import { setup, apiGet } from './setup'
import { expect } from 'chai'

describe('Organizations API', function() {
  setup(this)

  it('should return all organizations', async () => {
      const organizationsResponse = await apiGet('/organizations')
      expect(organizationsResponse.data.length).to.eql(8)
  })

})