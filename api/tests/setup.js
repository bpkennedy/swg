import axios from 'axios'
import { database, dropTables } from '../src/db'
import * as server from '../src'

const apiBaseUrl = 'http://localhost:3000/api/v1'
process.env.NODE_ENV = 'test'

export const apiGet = async (path) => {
  return await axios.get(apiBaseUrl + path)
}

export const apiPut = async (path) => {
  return await axios.put(apiBaseUrl + path)
}

export const setup = () => {
  beforeEach(async () => {
    await database().migrate.rollback()
    await database().seed.run()
    await database().migrate.latest()
  })
  afterEach(async () => {
    await database().migrate.rollback()
    await dropTables()
  })
}