import { setup, apiGet, apiPut } from './setup'
import { expect } from 'chai'

describe('Planets API', function() {
  setup(this)

  it('should return all planets', async () => {
      const planetsResponse = await apiGet('/planets')
      expect(planetsResponse.data.length).to.eql(75)
  })

  it('should return planets with correct properties', async () => {
    const planetsResponse = await apiGet('/planets')
    expect(planetsResponse.data[0].name).to.eql('Prakith')
  })
  
  it('should return single planet with government and organizations', async () => {
    const planetsResponse = await apiGet('/planets/1')
    expect(planetsResponse.data[0].name).to.eql('Prakith')
    expect(planetsResponse.data[0].government.name).to.eql('New Republic')
    expect(planetsResponse.data[0].organizations.length).to.eql(0)
  })
  
  it('should add organization to planet', async () => {
    const planetsResponse = await apiGet('/planets/1')
    expect(planetsResponse.data[0].organizations.length).to.eql(0)
    
    await apiPut('/planets/1/organization/1')
    const updatedPlanetsResponse = await apiGet('/planets/1')
    expect(updatedPlanetsResponse.data[0].organizations.length).to.eql(1)
    expect(updatedPlanetsResponse.data[0].organizations[0].id).to.eql(1)
  })

})