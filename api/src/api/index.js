import { apiVersion } from '../config.json'
import { Router } from 'express'
import planetsApi from './planets-api'
import organizationsApi from './organizations-api'

export default () => {
	let api = Router()

	api.use('/planets', planetsApi())
	api.use('/organizations', organizationsApi())

	api.get('/', (req, res) => {
		res.json({ apiVersion })
	})

	return api
}
