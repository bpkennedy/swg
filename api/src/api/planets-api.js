import { Router } from 'express'
import { Planet, PlanetOrganizations } from '../models/Planet'
import { celebrate, Joi } from 'celebrate'

const api = Router()

api.get('/', async (req, res) => {
	const planets = await Planet.query()
		.eager('[government]')
	res.send(planets)
})

api.get('/:id', celebrate({
	params: {
		id: Joi.string().required()
	}
}), async (req, res) => {
	const planet = await Planet.query()
		.eager('[government, organizations]')
		.where('id', req.params.id)
	res.send(planet)
})

api.put('/:planetId/organization/:organizationId', celebrate({
	params: {
		planetId: Joi.string().required(),
		organizationId: Joi.string().required(),
	}
}), async (req, res) => {
	const planetId = req.params.planetId
	const organizationId = req.params.organizationId
	const planetOrganization = await PlanetOrganizations.query()
		.where('planetId', planetId)
		.andWhere('organizationId', organizationId)
	if (planetOrganization.length === 0) {
		const updatedPlanet = await PlanetOrganizations.query()
			.insert({planetId, organizationId})
		res.send(updatedPlanet)
	} else {
		res.send(planetOrganization)
	}
})

export default () => { 
	return api
}
