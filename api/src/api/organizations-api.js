import { Router } from 'express'
import Organization from '../models/Organization'

const api = Router()

api.get('/', async (req, res) => {
	const orgs = await Organization.query()
		.eager('type')
	res.send(orgs)
})

export default () => { 
	return api
}
