import { Model } from 'objection'

class OrganizationType extends Model {
  static get tableName() {
    return 'organizationTypes'
  }
}

export default OrganizationType
