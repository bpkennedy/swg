import { Model } from 'objection'
import Organization from './Organization'

export class PlanetOrganizations extends Model {
  static get tableName() {
      return 'planetOrganizations'
  }
}

export class Planet extends Model {
  static get tableName() {
    return 'planets'
  }
  
  static get relationMappings() {
    return {
      organizations: {
        relation: Model.ManyToManyRelation,
        modelClass: Organization,
        join: {
          from: 'planets.id',
          through: {
            from: 'planetOrganizations.planetId',
            to: 'planetOrganizations.organizationId'
          },
          to: 'organizations.id'
        }
      },
      government: {
        relation: Model.HasOneRelation,
        modelClass: Organization,
        join: {
          from: 'planets.governmentId',
          to: 'organizations.id'
        }
      }
    }
  }
}
