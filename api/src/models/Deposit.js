import { Model } from 'objection'

class Deposit extends Model {
  static get tableName() {
    return 'deposits'
  }
}

export default Deposit
