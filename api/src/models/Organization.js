import { Model } from 'objection'
import OrganizationType from './OrganizationType'

class Organization extends Model {
  static get tableName() {
    return 'organizations'
  }

  static get relationMappings() {
    return {
      type: {
        relation: Model.HasOneRelation,
        modelClass: OrganizationType,
        join: {
          from: 'organizations.typeId',
          to: 'organizationTypes.id'
        }
      }
    }
  }
}

export default Organization
