import { Model } from 'objection'

const environment = process.env.NODE_ENV || 'development'
const configuration = require('../../knexfile')[environment]
const knex = require('knex')(configuration)
Model.knex(knex)

export const database = () => {
	return knex
}

export const dropTables = async () => {
	await knex.schema.dropTable('planets')
	await knex.schema.dropTable('organizations')
	await knex.schema.dropTable('organizationTypes')
}

export const initializeDb = async callback => {
	try {
		await knex.raw("SELECT 'test connection';")
		callback()
	} catch(error) {
		throw error
	}
}
