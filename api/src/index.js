import http from 'http'
import express from 'express'
import cors from 'cors'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import config from './config.json'
import { initializeDb } from './db'
import api from './api'

const app = express()
app.server = http.createServer(app)

app.use(morgan('dev'))

app.use(cors({
	exposedHeaders: config.corsHeaders
}))

app.use(bodyParser.json({
	limit : config.bodyLimit
}))

app.use(express.static('public'))

initializeDb(() => {
	app.use('/api/v1', api())
	app.server.listen(process.env.PORT || config.port, () => {
		console.log(`Started on port ${app.server.address().port}`)
		app.emit('apiStarted', null)
	})
})

export default app
